class HipToWaistCalculator {
    calculateWaitToHipRatio(male, hip, waist) {
        const hipWaistRatio = waist / hip;
        let category = "Normal weight";

        if (male) {
            if (hipWaistRatio >= 1) {
                category = "Obesity";
            } else if (hipWaistRatio >= 0.9) {
                category = "Over-weight";
            }

            return category;
        }

        if (hipWaistRatio >= 0.85) {
            category = "Obesity";
        } else if (hipWaistRatio >= 0.80) {
            category = "Over-weight";
        }

        return category;
    }
}

module.exports = () => new HipToWaistCalculator();