class BmiCategoryCalculator {
    calculateBmiCategory(bmi) {
        if(bmi <= 0) {
            throw Error("Not valid input")
        }

        if(bmi < 18.5) {
            return "Underweight";
        } else if(bmi < 25.0) {
            return "Healthy";
        } else if(bmi < 30) {
            return "Overweight";
        } else {
            return "Obese";
        }
    }
}

module.exports = () => new BmiCategoryCalculator();