const timeout = 5000;

describe("100 % decision test coverage", () => {
    let page;
    const pageUnderTest = 'http://localhost:8080/';

    beforeAll(async () => {
        page = await global.__BROWSER__.newPage();
        await page.goto(pageUnderTest, {"waitUntil": "networkidle0"}, timeout)
    });

    afterAll(async () => {
        await page.close();
    });

    it("Should fail on invalid height with input '0'", async () => {
        const height = "0",
            weight = "85";

        await page.evaluate((height, weight) => {
            const kg = document.querySelector('#kg');
            kg.value = weight;

            const cm = document.querySelector('#cm');
            cm.value = height;
        }, height, weight);

        await page.click("#submit");
        await page.waitFor(500);

        const kgInput = await page.evaluate(() => document.querySelector("#kg").value);
        const heightInput = await page.evaluate(() => document.querySelector("#cm").value);

        expect(kgInput).toBe("");
        expect(heightInput).toBe("")
    });

    it("Should fail on invalid height with input 'a'", async () => {
        const height = "a",
            weight = "85";

        await page.evaluate((height, weight) => {
            const kg = document.querySelector('#kg');
            kg.value = weight;

            const cm = document.querySelector('#cm');
            cm.value = height;
        }, height, weight);

        await page.click("#submit");
        await page.waitFor(500);

        const kgInput = await page.evaluate(() => document.querySelector("#kg").value);
        const heightInput = await page.evaluate(() => document.querySelector("#cm").value);

        expect(kgInput).toBe("");
        expect(heightInput).toBe("")
    });

    it("Should pass with input valid height and weight inputs h='185' and w='85'", async () => {
        const height = "185",
            weight = "85";

        await page.evaluate((height, weight) => {
            const kg = document.querySelector('#kg');
            kg.value = weight;

            const cm = document.querySelector('#cm');
            cm.value = height;
        }, height, weight);

        await page.click("#submit");
        await page.waitFor(500);

        const noteOutput = await page.evaluate(() => document.querySelector("#bmi").innerHTML);
        const expected = "24.84";

        expect(noteOutput).toBe(expected);
    });
});