const HipToWaistCalculator = require('../../../src/HipToWaistCalculator');

describe("Hip-to-Waist calculating unit tests", () => {
    let hipToWaistCalculator;

    beforeEach(() => {
        hipToWaistCalculator = HipToWaistCalculator();
    });

    it("should return 'Normal weight' for male with waist 62,5 and hip 70", () => {
        const waist = 62.5,
            hip = 70;

        const expected = "Normal weight";
        const actual = hipToWaistCalculator.calculateWaitToHipRatio(true, hip, waist);

        expect(actual).toBe(expected);
    });

    it("should return 'Over-weight' for male with waist 63 and hip 70", () => {
        const waist = 63,
            hip = 70;

        const expected = "Over-weight";
        const actual = hipToWaistCalculator.calculateWaitToHipRatio(true, hip, waist);

        expect(actual).toBe(expected);
    });

    it("should return 'Over-weight' for male with waist 69.5 and hip 70", () => {
        const waist = 69.5,
            hip = 70;

        const expected = "Over-weight";
        const actual = hipToWaistCalculator.calculateWaitToHipRatio(true, hip, waist);

        expect(actual).toBe(expected);
    });

    it("should return 'Obesity' for male with hip 70 and waist 70", () => {
        const waist = 70,
            hip = 70;

        const expected = "Obesity";
        const actual = hipToWaistCalculator.calculateWaitToHipRatio(true, hip, waist);

        expect(actual).toBe(expected);
    });

    it("should return 'Normal weight' for female with hip 55,5 and waist 70", () => {
        const waist = 55.5,
            hip = 70;

        const expected = "Normal weight";
        const actual = hipToWaistCalculator.calculateWaitToHipRatio(false, hip, waist);

        expect(actual).toBe(expected);
    });

    it("should return 'Over-weight' for female with hip 56 and waist 70", () => {
        const waist = 56,
            hip = 70;

        const expected = "Over-weight";
        const actual = hipToWaistCalculator.calculateWaitToHipRatio(false, hip, waist);

        expect(actual).toBe(expected);
    });

    it("should return 'Over-weight' for female with hip 59 and waist 70", () => {
        const waist = 59,
            hip = 70;

        const expected = "Over-weight";
        const actual = hipToWaistCalculator.calculateWaitToHipRatio(false, hip, waist);

        expect(actual).toBe(expected);
    });

    it("should return 'Obesity' for female with hip 60 and waist 70", () => {
        const waist = 60,
            hip = 70;

        const expected = "Obesity";
        const actual = hipToWaistCalculator.calculateWaitToHipRatio(false, hip, waist);

        expect(actual).toBe(expected);
    });
});