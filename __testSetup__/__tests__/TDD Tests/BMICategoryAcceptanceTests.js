const timeout = 5000;

describe("Acceptance tests for BMI category", () => {
    let page;
    const pageUnderTest = 'http://localhost:8080/';

    beforeEach(async () => {
        page = await global.__BROWSER__.newPage();
        await page.goto(pageUnderTest, {"waitUntil": "networkidle0"}, timeout)
    });

    afterAll(async () => {
        await page.close();
    });

    it("should display 'Underweight' BMI-category on valid BMI-value", async () => {
        const weight = "60", // BMI = 16.62
            height = "190";

        await page.evaluate((height, weight) => {
            const kg = document.querySelector('#kg');
            kg.value = weight;

            const cm = document.querySelector('#cm');
            cm.value = height;
        }, height, weight);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Underweight";
        const actualCategory = await page.evaluate(() => document.querySelector("#bmi-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Healthy' BMI-category on valid BMI-value", async () => {
        const weight = "75", // BMI = 24.49
            height = "175";

        await page.evaluate((height, weight) => {
            const kg = document.querySelector('#kg');
            kg.value = weight;

            const cm = document.querySelector('#cm');
            cm.value = height;
        }, height, weight);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Healthy";
        const actualCategory = await page.evaluate(() => document.querySelector("#bmi-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Overweight' BMI-category on valid BMI-value", async () => {
        const weight = "80", // BMI = 27.68
            height = "175";

        await page.evaluate((height, weight) => {
            const kg = document.querySelector('#kg');
            kg.value = weight;

            const cm = document.querySelector('#cm');
            cm.value = height;
        }, height, weight);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Overweight";
        const actualCategory = await page.evaluate(() => document.querySelector("#bmi-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Obese' BMI-category on valid BMI-value", async () => {
        const weight = "90", // BMI = 31.14
            height = "170";

        await page.evaluate((height, weight) => {
            const kg = document.querySelector('#kg');
            kg.value = weight;

            const cm = document.querySelector('#cm');
            cm.value = height;
        }, height, weight);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Obese";
        const actualCategory = await page.evaluate(() => document.querySelector("#bmi-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Invalid' BMI-category on invalid BMI-value", async () => {
        const weight = "0", // BMI = 0
            height = "185";

        await page.evaluate((height, weight) => {
            const kg = document.querySelector('#kg');
            kg.value = weight;

            const cm = document.querySelector('#cm');
            cm.value = height;
        }, height, weight);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Invalid";
        const actualCategory = await page.evaluate(() => document.querySelector("#bmi-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });
});