const timeout = 5000;

describe("Acceptance tests for hip-to-waist category", () => {
    let page;
    const pageUnderTest = 'http://localhost:8080/hip-waist';

    beforeEach(async () => {
        page = await global.__BROWSER__.newPage();
        await page.goto(pageUnderTest, {"waitUntil": "networkidle0"}, timeout)
    });

    it("should display 'Normal weight' hip-to-waist category on valid input gender male, waist 62,5 and hip 70", async () => {
        const waist = "62.5",
            hip = "70";

        await page.evaluate((waist, hip) => {
            const waistTag = document.querySelector('#waist');
            waistTag.value = waist;

            const hipTag = document.querySelector('#hip');
            hipTag.value = hip;

            const genderTag = document.querySelector('#male');
            genderTag.checked = true;
        }, waist, hip);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Normal weight";
        const actualCategory = await page.evaluate(() => document.querySelector("#hipWaist-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Over-weight' hip-to-waist category on valid input gender male, waist 63 and hip 70", async () => {
        const waist = "63",
            hip = "70";

        await page.evaluate((waist, hip) => {
            const waistTag = document.querySelector('#waist');
            waistTag.value = waist;

            const hipTag = document.querySelector('#hip');
            hipTag.value = hip;

            const genderTag = document.querySelector('#male');
            genderTag.checked = true;
        }, waist, hip);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Over-weight";
        const actualCategory = await page.evaluate(() => document.querySelector("#hipWaist-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Over-weight' hip-to-waist category on valid input gender male, waist 69,5 and hip 70", async () => {
        const waist = "69.5",
            hip = "70";

        await page.evaluate((waist, hip) => {
            const waistTag = document.querySelector('#waist');
            waistTag.value = waist;

            const hipTag = document.querySelector('#hip');
            hipTag.value = hip;

            const genderTag = document.querySelector('#male');
            genderTag.checked = true;
        }, waist, hip);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Over-weight";
        const actualCategory = await page.evaluate(() => document.querySelector("#hipWaist-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Obesity' hip-to-waist category on valid input gender male, waist 70 and hip 70", async () => {
        const waist = "70",
            hip = "70";

        await page.evaluate((waist, hip) => {
            const waistTag = document.querySelector('#waist');
            waistTag.value = waist;

            const hipTag = document.querySelector('#hip');
            hipTag.value = hip;

            const genderTag = document.querySelector('#male');
            genderTag.checked = true;
        }, waist, hip);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Obesity";
        const actualCategory = await page.evaluate(() => document.querySelector("#hipWaist-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Normal weight' hip-to-waist category on valid input gender female, waist 55,5 and hip 70", async () => {
        const hip = "70",
            waist = "55.5";

        await page.evaluate((waist, hip) => {
            const waistTag = document.querySelector('#waist');
            waistTag.value = waist;

            const hipTag = document.querySelector('#hip');
            hipTag.value = hip;
        }, waist, hip);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Normal weight";
        const actualCategory = await page.evaluate(() => document.querySelector("#hipWaist-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Over-weight' hip-to-waist category on valid input gender female, waist 56 and hip 70", async () => {
        const hip = "70",
            waist = "56";

        await page.evaluate((waist, hip) => {
            const waistTag = document.querySelector('#waist');
            waistTag.value = waist;

            const hipTag = document.querySelector('#hip');
            hipTag.value = hip;
        }, waist, hip);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Over-weight";
        const actualCategory = await page.evaluate(() => document.querySelector("#hipWaist-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Over-weight' hip-to-waist category on valid input gender female, waist 59 and hip 70", async () => {
        const hip = "70",
            waist = "59";

        await page.evaluate((waist, hip) => {
            const waistTag = document.querySelector('#waist');
            waistTag.value = waist;

            const hipTag = document.querySelector('#hip');
            hipTag.value = hip;
        }, waist, hip);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Over-weight";
        const actualCategory = await page.evaluate(() => document.querySelector("#hipWaist-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });

    it("should display 'Obesity' hip-to-waist category on valid input gender female, waist 60 and hip 70", async () => {
        const hip = "70",
            waist = "60";

        await page.evaluate((waist, hip) => {
            const waistTag = document.querySelector('#waist');
            waistTag.value = waist;

            const hipTag = document.querySelector('#hip');
            hipTag.value = hip;
        }, waist, hip);

        await page.click("#submit");
        await page.waitFor(500);

        const expected = "Obesity";
        const actualCategory = await page.evaluate(() => document.querySelector("#hipWaist-category").innerHTML);

        expect(actualCategory).toBe(expected);
    });
});