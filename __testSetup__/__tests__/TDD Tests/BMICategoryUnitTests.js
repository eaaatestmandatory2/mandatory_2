const BmiCategoryCalculator = require("../../../src/BmiCategoryCalculator");

describe("BMI Category unit tests", () =>{
    let bmiCategoryCalculator;

    beforeAll(() => {
        bmiCategoryCalculator = BmiCategoryCalculator();
    });

    it("should return 'Underweight'-category on BMI-input 18.4", () => {
        const recievedCategory = bmiCategoryCalculator.calculateBmiCategory(18.4);
        const expected = "Underweight";

        expect(recievedCategory).toBe(expected);
    });

    it("should return 'Healthy'-category on BMI-input 18.5", () => {
        const recievedCategory = bmiCategoryCalculator.calculateBmiCategory(18.5);
        const expected = "Healthy";

        expect(recievedCategory).toBe(expected);
    });

    it("should return 'Healthy'-category on BMI-input 24.9", () => {
        const recievedCategory = bmiCategoryCalculator.calculateBmiCategory(24.9);
        const expected = "Healthy";

        expect(recievedCategory).toBe(expected);
    });

    it("should return 'Overweight'-category on BMI-input 25", () => {
        const recievedCategory = bmiCategoryCalculator.calculateBmiCategory(25);
        const expected = "Overweight";

        expect(recievedCategory).toBe(expected);
    });

    it("should return 'Overweight'-category on BMI-input 29.9", () => {
        const recievedCategory = bmiCategoryCalculator.calculateBmiCategory(29.9);
        const expected = "Overweight";

        expect(recievedCategory).toBe(expected);
    });

    it("should return 'Obese'-category on BMI-input 30.0", () => {
        const recievedCategory = bmiCategoryCalculator.calculateBmiCategory(30);
        const expected = "Obese";

        expect(recievedCategory).toBe(expected);
    });

    it("should throw error on invalid BMI-input 0",() => {
        expect(() => {
            bmiCategoryCalculator.calculateBmiCategory(0)
        }).toThrow("Not valid input");
    });
});