var express = require("express");
const bmiCategoryCalculator = require('./src/BmiCategoryCalculator')();
const hipWaistCalculator = require('./src/HipToWaistCalculator')();

// create express app
var app = express();

app.set("view engine", "ejs");
app.set("views", __dirname + "/views");

app.use("/public", express.static("public"));

app.get("/", (req, res) => {
    res.render("pages/landing");
});

app.get('/hip-waist', (req, res) => {
    res.render("pages/hipWaist")
});

app.get('/hipWaistResult', (req, res) => {
    const hip = (parseFloat((req.query.hip))),
        waist = (parseFloat((req.query.waist))),
        male = !!req.query.male;

    if(!hip || !waist || hip <= 0 || waist <= 0) {
        res.redirect('/hip-waist')
    }

    const hipWaistCategory = hipWaistCalculator.calculateWaitToHipRatio(male, hip, waist);

    res.render("pages/hipWaistResult", {
        category: hipWaistCategory
    });
});

app.get("/result", (req, res) => {
    var m = (parseInt(req.query.cm, 10) / 100);
    var kg = req.query.kg;

    if (m == 0 || isNaN(m)) {
        res.redirect("/");
    } else {
        var bmi = (kg / (m * m));

        const bmiCategory = bmi <= 0 ? "Invalid" : bmiCategoryCalculator.calculateBmiCategory(bmi);

        res.render("pages/result", {
            bmi: bmi,
            category: bmiCategory
        });
    }
});

var port = 8080;
console.log("App is running on http://localhost:" + port)
app.listen(port);
